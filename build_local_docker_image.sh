set -e
image_version=$(date +%s) # Every image has a repository and a tag.
mvn clean compile package
docker image build --tag hello_spring_boot:$image_version .
