## What is CloudWatch?
Cloudwatch is the resource and application monitoring service of the AWS.

The applications developed on the AWS platform can collect logs and metrics.
These metrics and logs can be used for monitoring and alerting purposes. 
Through interaction of certain AWS services such as ECS, EC2, etc. certain metrics can be automatically monitored by CloudWatch and once a certain criteria such as a threshold of CPU usage has been reached, certain automated actions can be taken.



## How can application logs can be viewed on CloudWatch?
Many services such as AWS Lambda, API Gateway, Sagemaker, ECS etc. automatically create _log groups_ in the Cloudwatch. 
Log groups are further divided into _log streams_.

For example, if you're developing a Spring application and running it on ECS, a log group on Cloudwatch would automatically be created.
Every task run of the spring application would have a separate log stream with the timestamp as the name. 
Your log4j logs would automatically appear inside the log stream.

