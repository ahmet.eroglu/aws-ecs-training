# EC2
* Simply a virtual machine on the cloud
  * Yet, has other supporting services as well. 
* Can be on Linux, Windows or Mac on CPU architectures ARM and X64.

## EBS (Elastic Block Storage)
Elastic block storage is the primary storage of an EC2 instance.
It's mounted to your EC2 instance as either an HDD or an SSD.

### For which purposes can EBS be used?
EBS can be used for any purpose a hard drive of a computer can be used for. 
From simple file storage to serving of enterprise software such as relational databases.