# Regions
AWS infrastructure is available in certain locations all around the globe. In Germany there is an AWS region in Frankfurt.
Each region is divided into Availability Zones

## Availability Zones
Availability zones share a zone, for example in Frankfurt (eu-central-1) region, there are three Availability zones "eu-central-1a", "eu-central-1b", "eu-central-1c".
Each availability zone inside a region has complete separate infrastructure, which allows them to operate completely independently. 
If a disaster occurs in one of the availability zones such as electricity issues, earthquake etc., since the AZs have quite a lot of distance between them the other wouldn't be affected.
In many services such as ECS, your application is usually deployed in multiple AZs within the same region, if an AZ isn't available, another AZ acts as redundance, ensuring _high-availability_. 