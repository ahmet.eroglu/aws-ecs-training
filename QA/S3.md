# S3
  * Simple Storage Service
  * Object Storage
  * Used for storing any sort of files; structured, semi-structured, non-structured.
  * Organized as buckets.
  * Every object is a file in a bucket
  * Example
    * Located in the bucket `qimia-data-eng-training`
      * object URI: `s3://qimia-data-eng-training/nyc-taxi-data/greentaxi_tripdata/from=2019-01-01/data.csv` 
      * Its ARN: `arn:aws:s3:::qimia-data-eng-training/nyc-taxi-data/greentaxi_tripdata/from=2019-01-01/data.csv`
  * Access permission model is either through IAM, S3 Bucket Policies or ACL (old).

### IAM Policies
* The access is proved by the policies attached to the users or roles.
* The same way of accessing every other AWS resource such as Secrets Manager secrets.
* https://aws.amazon.com/blogs/security/iam-policies-and-bucket-policies-and-acls-oh-my-controlling-access-to-s3-resources/
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
              "arn:aws:s3:::test-sample-bucket",
              "arn:aws:s3:::test-sample-bucket/*"
            ]
        }
    ]
}
```
* The policy above allows all the actions to both the bucket itself and any other object within it. 
* This access can be made more granular, such  as specifying `"arn:aws:s3:::test-sample-bucket/dir1/"` instead of `"arn:aws:s3:::test-sample-bucket/*`, then the bearer of the policy would have access to only the contents of the dir1 directory. 

### S3 Bucket Policy
* The access policies are attached to the S3 bucket instead of the user or role. 
* Decides who has access to its resources in its principal field.
```json
{
  "Id": "Policy1586088133829",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1586088060284",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::test-sample-bucket",
      "Principal": {
        "AWS": [
          "arn:aws:iam::112233445566:root",
          "arn:aws:iam::112233445566:user/Tom"
        ]
      }
    }
  ]
}
```
In this case, the user Tom and the root user have access to the bucket `test-sample-bucket`.

* How to access S3 object list?
  * Use the Web portal.
  * Bash command `aws s3api list-buckets` lists the buckets.
  * `aws s3api list-objects --bucket qimia-data-eng-training` for listing all the objects within the bucket recursively in json format.
    * or `aws s3 ls s3://qimia-data-eng-training` for human readable listing.
  * `aws s3 cp s3://qimia-data-eng-training/nyc-taxi-data/greentaxi_tripdata/from=2019-01-01/data.csv data.csv` to download a file from 3

## AWS Conditions
Example role policy, attach it to a user so th
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "RunInstance",
            "Effect": "Allow",
            "Action": "ec2:RunInstances",
            "Resource": "*",
            "Condition": {
                "StringLikeIfExists": {
                    "ec2:InstanceType": [
                        "t1.*",
                        "t2.*",
                        "m3.*"
             ]}}
        }
}
```