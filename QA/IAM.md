* Root user
  * The user that _owns_ the AWS organization account.
  * Eg the business owner
  * Should mostly be used for creating other Administrator accounts (IAM users) only.
* IAM user
  * The users that are created under the root user assigned to other people
* Policy
  * A policy is a _document_ of some sorts that specifies granting a certain action(s) on certain resource(s).
  * It doesn't yet define what service can performs these actions.
  * Must be assigned to someone - a real user, or a role to be used.
  * Eg. "_s3:GetObject permission on all resources of arn:aws:s3:::qimia-data-eng-training/nyc-taxi-data/*._"
  * Defined as JSONs
* Customer Managed Policy
  * A manually defined policy that is defined by the customer itself.
* AWS Managed Policy
  * A pre-created policy by AWS, as they're used commonly.
* Role
  * A virtual user of some sorts that are used by services such as EC2 instances to access other resources.
  * A role has policies attached to it that defines the role's user's permissions. 
  * A service assumes the role and has the permissions defined in the attached policies.
  * What service can assume a role is defined inside its trust relationship.
* User group
  * Group of users that inherit the policies attached to the group.
* Instance Profile
  * Basically a role that is granted for EC2 _instances_.
* How to give a certain service, access to a resource?
  1. Create policy / policies that specify a permission to perform action(s) on resource(s).
     * Give as little permissions as it's viable. Don't give any unneeded permissions.
  2. Create a role, attach the created policies to the role.
  3. Edit the role's trust relationship so that the service is allowed to assume the role.
  4. Assign the role to the service's profile.
* How to grant access to a user?
  1. Create policy / policies that specify a permission to perform action(s) on resource(s).
    * Give as little permissions as it's viable. Don't give any unneeded permissions.
  2. If the user can be grouped with others, add them to a group, attach the policies to the group.
  3. If (2) isn't applicable, attach the policy directly to the user itself.
* What are access keys?
  * A pair of access key id and an access key secret.
  * Used for programmatic access to AWS. 

