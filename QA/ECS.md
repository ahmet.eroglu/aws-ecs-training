ECS is the container cluster service of AWS. 
## How do you create an ECS cluster?
1. Create your application and create a docker image from it.
2. Create a repository on AWS Elastic Container Registry.
3. Tag your docker image as "<DOCKER_REPOSITORY>:<VERSION_TAG>".
4. Ideally, you should be using AWS CLI for infrastructure as code.
5. You should first login to the ECR. 
6. Upload the docker image that you just tagged with docker push. Docker's going to upload the image to the repository in the tagged image's name with the specified version tag.
7. Create a **task execution role** on AWS IAM using either the CLI or the web console.
  * ```json 
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "",
          "Effect": "Allow",
          "Principal": {
            "Service": "ecs-tasks.amazonaws.com"
          },
          "Action": "sts:AssumeRole"
        }
      ]
    }
    ```
  * The role policy assume document should look like above. If you're using EC2 instead of Fargate, the service above should be `ecs.amazonaws.com`
  * This role is necessary for ECS to pull docker images from ECR and allow it to write logs to CloudWatch.
8. Create a **task role**. Not to be confused with the task execution role. 
  * The task role is used by ECS containers to access AWS resources from your application.
  * For example if your Spring Java application is using the AWS Java SDK to access resources such as retrieving secrets from Secret Manager or downloding files from S3, then your ECS clusters will use this role.   
    * Retrieving secret values from the Secret Manager.
    * Accessing data on S3 etc. 
9. Setup your ECS CLI by creating a CLI profile using the AWS access key id and secret.
10. Create a local cluster config with a cluster name, region and type (eg. FARGATE)
    1. This is purely local and is used in other ECS CLI commands for taking the cluster name, region and type automatically
11. Use the `ecs cli up` command to create the cluster, if a VPC information is not given to the command, a VPC will be created and its ID and subnets IDs will be returned as well. Note these down.
12. Find the security group of the ECS cluster you just created.
13. Allow the ports 80 and 8080 in the security group ingress (inbound rules). 
    1. This will basically allow remote clients outside your AWS VPC to access the ECS. Such as your web browser.
14. Create a docker compose that's using the image on the ECR.
15. Use the ecs cli to start the FARGATE cluster service.
    1. `ecs-cli compose --project-name YOURSERVICENAME service up  --create-log-groups --cluster-config oguzTutorialConfig --ecs-profile default`
16. Scale up the FARGATE cluster service.
    1. `ecs-cli compose --project-name YOURSERVICENAME service  up --create-log-groups --cluster-config oguzTutorialConfig --ecs-profile default`

## Why use ECS?
ECS lets you easily create clusters out of Docker containers by eliminating the need to manage the cluster infrastructure.
It lets you simply use Docker container with all the dependencies of your application installed inside.
Your application and can also run _cross-cloud_ meaning it can have less cloud specific code which you can deploy to other cloud platforms more easily. 

## What can you implement with ECS?
The ideal use case is creating multi-container docker clusters for parallel tasks.
Examples can be:
* Typically, Microservices.
* Web service backends that serve as an endpoint to the frontend clients.
* Batch data processing clusters, such as Apache Spark where each node is either a Spark master or a Spark worker. 
* Stream processing clusters such as Apache Kafka clusters with Schema Registry, brokers, Zookeepers, etc. 
* In many of these cases, consider also using the specifically developed services on AWS for purposes.
  * Using AWS Lambda or Beanstalk for web backend development using just the jar files, eliminating the Docker images.
  * Using data processing clusters such as EMR or more managed services such as AWS Glue. 
  * RDS instead of putting the database on an EC2 instance or an ECS cluster.

## What are Tasks in ECS?
Task is a Docker Container on ECS. 

## What are Task definitions on ECS?
ECS contains the container definitions and service definition for the ECS cluster.

## How does a Load Balancer work?
A load balancer is a layer that receives the incoming traffic from a client such as a web browser and forwards it to the target services such as an ECS cluster.
This way multiple underlying web services can be run at the same time in parallel and the clients have only one point of contact

## What is Auto Scaling?
Auto scaling is the act of increasing or decreasing the number of running tasks in your services.
In ECS, there are two recommended ways of doing this:

### Target Tracking Scaling Policies
Use CPU utilization, memory utilization, or application load balancer request count per target to scale your cluster in and out.
Let's say you want to auto scale based on CPU usage. 
Cloudwatch already tracks these under its _metrics_. 
After creating scale in and scale out policies, when criteria such as high CPU usage or sustained low CPU usage are put in place, these metrics will trigger Cloudwatch alerts, ECS will pick them up and scale accordingly.

### Schedule based Scaling
AWS CLI has a command `aws application-autoscaling put-scheduled-action`. This can be used to scale in and scale out your cluster at certain intervals.
Let's say your website receives heavy traffic between 9:00-18:00, then you can set a schedule to scale out the cluster at 8:50 and scale in at 18:30.
