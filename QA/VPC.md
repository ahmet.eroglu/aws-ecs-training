# VPC - Virtual Private Cloud
VPC is a virtual network that your AWS resources are placed in. 
It resembles a workplace network where all the computers are placed in a certain network and some are restricted from internet access.

## Why are VPCs used?
VPCs are mainly used for protecting the created resources inside an AWS cluster and sometimes for simplifying the communication within VPC.
The VPCs either completely isolate or only allow certain outside access to its resources, therefore, it acts as sort of a firewall that blocks unwanted internet access.

### What are subnets?
The resources created in AWS are given local IPs within defined range.  
These ranges correspond to Subnets. For example:
10.0.0.0/24: corresponds to IP range from 10.0.0.0 to 10.0.0.255. The first 24 bits are fixed.

### Public Subnets
If resources inside an IP range are allowed to access external access, then they're called public subnets.
However, this access is restricted with security groups. 
These subnets are for resources that need external clients to communicate with, such as web servers. 
The resources in the public subnets are assigned public IPv4 address using the Elastic IP addresses.


### Private Subnets
If resources inside an IP range are _not_ allowed to access external access, then they're called private subnets.
Private subnets are mostly for resources that typically need no internet access and often, exposing them would pose security risk. 
An example would be relational databases such as MySQL.

### Security Groups
A security group basically defines a list of rules that allow explicit communication.
Each resource is assigned to a security group and these resources are allowed network access only on these listed rules.
Each security group is defined under a VPC.
A rule consists of a combination of IP type, IP range, protocol, port range. 
Or another security group instead of an IP/port range.
There are separate rule sets for inbound and outbound traffic also called ingress and egress respectively.
* Inbound traffic refers to the communication that first originates from outside into the resource and by default allows nothing.
* Outbound traffic refers to the communication that first originates from the resource to outside and by default allows everything.

These rules apply not only for communicating with outside the VPC but also communication within the VPC.

An example setup could be that a webserver running inside ECS on a public subnet and an RDS database on private subnet. 
* An ingress rule that allow HTTPS requests from any IP address on the port 8080 which allows the ECS to retrieve web requests.
* Another ingress rule that allow all traffic with any other resource in the same security group.


ECS wants to access RDS, ECS is in subnet group SG1, and RDS has subnet group SG2.
1. SG2 may be a private subnet but SG1 must be a public subnet.
2. SG1 needs to allow internet traffic in therefore it has to include an inbound rule that accepts requests from any IP on TCP HTTP on a specific port at least.
3. ECS needs to communicate with RDS, therefore
   1. The outbound rules of SG1 need to allow communication to SG1 or specifically the local IP of RDS.
   2. The inbound rules of the SG2 need to accept requests either from all traffic from SG1 or the IP addresses of ECS instances.