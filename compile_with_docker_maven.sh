# We first download the official docker image to our local repository
# https://hub.docker.com/_/maven
docker pull maven
cwd=$(pwd)
(docker stop maven_spring_boot_hello || exit 0) && (docker rm maven_spring_boot_hello || exit 0)

docker run --name maven_spring_boot_hello  -it -v "$cwd/:/code" maven:latest /bin/bash -c "cd code; mvn clean compile package"
