package com.testing;

import org.json.JSONException;

public interface SecretFetcher {
    String fetchSecretFromAws(String secretKey) throws JSONException;
}
