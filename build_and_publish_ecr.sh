set -e
if [ -z "$1" ] # Make sure an argument is given. The argument must be the name of your repository on ECR.
  then
    echo "Make sure an argument is given. The argument must be the name of your repository on ECR. "
    exit 1
fi
image_name=$1

image_version=$2 # Use the provided repo version.
if [ -z "$2" ]
  then
    image_version=$(date +%s) # No image version was given, autogenerating it with the timestamp
fi


# aws configure # Ask you questions like secret key id and secret region, format like json

# See https://docs.aws.amazon.com/AmazonECR/latest/userguide/docker-push-ecr-image.html
aws ecr get-login-password --region eu-central-1 --profile default | docker login --username AWS --password-stdin 906856305748.dkr.ecr.eu-central-1.amazonaws.com/

# compile the application and build the local Docker image
mvn clean compile package # Build the jar
docker image build --tag hello_spring_boot:$image_version . # Build the docker image from the Dockerfile

# Tag the Docker image we just created with the URI of the ECR so that we can upload it
docker tag "hello_spring_boot:$image_version" "hello_spring_boot:latest"
docker tag "hello_spring_boot:$image_version" "906856305748.dkr.ecr.eu-central-1.amazonaws.com/$image_name:$image_version"
docker tag "hello_spring_boot:$image_version" "906856305748.dkr.ecr.eu-central-1.amazonaws.com/$image_name:latest"

# Upload/push the tagged image to ECR.
docker push "906856305748.dkr.ecr.eu-central-1.amazonaws.com/$image_name:$image_version"
docker push "906856305748.dkr.ecr.eu-central-1.amazonaws.com/$image_name:latest"
