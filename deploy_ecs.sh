#/bin/bash

# Create a role called helloEcsTaskExecutionRole, you should change this
aws iam --region eu-central-1 create-role --role-name helloEcsTaskExecutionRole --assume-role-policy-document file://task-execution-assume-role.json
# Assign a policy to role
aws iam --region eu-central-1 attach-role-policy --role-name helloEcsTaskExecutionRole --policy-arn arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy
aws iam --region eu-central-1 attach-role-policy --role-name helloEcsTaskExecutionRole --policy-arn arn:aws:iam::906856305748:policy/dataeng-bucket-access

# Setting up the credentials of the ECS cli so that it can communicate with the AWS, only need to run it once.

## For configuring our local ECS installation
# first create a cluster config locally. I created a config called oguzTutorialConfig that uses a cluster called oguzHelloTutorial by default.
ecs-cli configure --cluster ahmetHelloTutorial --default-launch-type FARGATE --config-name ahmetTutorialConfig --region eu-central-1
# ecs-cli configure profile --access-key AWS_ACCESS_KEY_ID --secret-key  AWS_SECRET_KEY --profile-name default


##Save the output of this step
ecs-cli up --cluster-config ahmetTutorialConfig --ecs-profile default --vpc vpc-0bdd0b03c9d9fb0e1 --subnets subnet-096488e2247cd6be6,subnet-035a8358d4b0008de
# VPC created: vpc-0bdd0b03c9d9fb0e1
# Subnet created: subnet-096488e2247cd6be6
# Subnet created: subnet-035a8358d4b0008de


exit 0


# Replace the vpc parameter and then note the security group id.
aws ec2 describe-security-groups --filters Name=vpc-id,Values=vpc-0bdd0b03c9d9fb0e1 --region eu-central-1

aws ec2 authorize-security-group-ingress --group-id sg-0e40ba5286ce98eaa --protocol tcp --port 8080 --cidr 0.0.0.0/0 --region eu-central-1
aws ec2 authorize-security-group-ingress --group-id sg-0e40ba5286ce98eaa --protocol tcp --port 80 --cidr 0.0.0.0/0 --region eu-central-1

ecs-cli compose --project-name multinodeHelloWorld service  up --create-log-groups --cluster-config ahmetTutorialConfig --ecs-profile default
ecs-cli compose --project-name multinodeHelloWorld service scale 4 --cluster-config ahmetTutorialConfig --ecs-profile default
